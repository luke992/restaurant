package Lukescode;

import java.sql.*;
import java.util.ArrayList;

public class Menu {

    private int id;
    private String title;
    private int restaurant_id;
    public static ArrayList<Menu> all = new ArrayList<>();

    public static void getOneMenu() {
        try {
            PreparedStatement getOneMenu = DB.conn.prepareStatement("SELECT * FROM menus WHERE id = ?;");
            getOneMenu.setInt(1, 4);
            ResultSet chosenMenu = getOneMenu.executeQuery();
            System.out.printf("Chosen menu is %s\n", chosenMenu.getString(3));
        } catch (SQLException err) {
        }
    }

    // DELETE
    public static void deleteOneMenu() {
        try {
            PreparedStatement deleteOneMenu = DB.conn.prepareStatement("DELETE FROM menus WHERE id = ?;");
            deleteOneMenu.setInt(1, 11);
            deleteOneMenu.executeUpdate();

        } catch (SQLException err) {

        }
    }

    // Select all from restaurant table
    public static void getAllMenus() {
        try {
            Statement getAllMenus = DB.conn.createStatement();
            ResultSet results = getAllMenus.executeQuery("Select * FROM menus;");
            while (results.next()) {
                System.out.printf(" id: %d\n", results.getInt(1));
                System.out.printf(" restaurant_id %d\n", results.getInt(2));
                System.out.printf("title %s\n", results.getString(3));
            }
        } catch (SQLException errr) {
        }
    }

    // Create a new table if doesnt exist
    public static void init() {
        Statement createTable;
        try {
            createTable = DB.conn.createStatement();
            createTable.execute(
                    "CREATE TABLE IF NOT EXISTS menus (id INTEGER PRIMARY KEY, restaurant_id INTEGER, title TEXT);");
            Statement getMenu = DB.conn.createStatement();
            ResultSet menu = getMenu.executeQuery("Select * From menus;");
            while (menu.next()) {
                int id = menu.getInt(1);
                int restaurant_id = menu.getInt(2);
                String title = menu.getString(3);
                new Menu(id, restaurant_id, title);
            }

        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
    }

    // Constructor for menu
    public Menu(int restaurant_id, String title) {
        this.title = title;
        this.restaurant_id = restaurant_id;
        try {
            PreparedStatement insertmenu = DB.conn
                    .prepareStatement("INSERT INTO menus (restaurant_id,title) VALUES (?,?);");
            insertmenu.setInt(1, this.restaurant_id);
            insertmenu.setString(2, this.title);
            insertmenu.executeUpdate();
            this.id = insertmenu.getGeneratedKeys().getInt(1);
        } catch (SQLException err) {

        }
        Menu.all.add(this);
    }

    public Menu(int id, int restaurant_id, String title) {
        this.id = id;
        this.restaurant_id = restaurant_id;
        this.title = title;
        Menu.all.add(this);
    }

    public int getMenuID() {
        return this.id;

    }

    public String getTitle() {
        return title;
    }

}
