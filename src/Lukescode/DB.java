package Lukescode;
import java.sql.*;

public class DB {
public static Connection conn;

    public DB(String connectionString) {
        try {
            DB.conn = DriverManager.getConnection(connectionString);
        } catch (SQLException error) {
            System.out.println(error.getMessage());

        }
    }

    public Connection getConnection() {
        return DB.conn;
    }
       
    public static void close() {
        try {
            DB.conn.close();
            
        } catch (Exception error) {
            System.out.println(error.getMessage());
        }
    }

        
}


