package Lukescode;

import java.sql.*;
import java.util.ArrayList;

public class Item {
    private int menu_id;
    private String name;
    private Double price;
    private int id;
    public static ArrayList<Item> all = new ArrayList<>();

    public static void getOneItem() {
        try {
            PreparedStatement getOneItem = DB.conn.prepareStatement("SELECT * FROM items WHERE id = ?;");
            getOneItem.setInt(1, 4);
            ResultSet chosenItem = getOneItem.executeQuery();
            System.out.printf("Chosen Item is %s\n", chosenItem.getString(3));
        } catch (SQLException err) {
        }
    }

    // DELETE
    public static void deleteOneItem() {
        try {
            PreparedStatement deleteOneItem = DB.conn.prepareStatement("DELETE FROM items WHERE id = ?;");
            deleteOneItem.setInt(1, 11);
            deleteOneItem.executeUpdate();
        } catch (SQLException err) {
        }
    }

    // Select all from restaurant table
    public static void getAllItems() {
        try {
            Statement getAllItems = DB.conn.createStatement();
            ResultSet results = getAllItems.executeQuery("Select * FROM items;");
            while (results.next()) {
                System.out.printf(" id: %d\n", results.getInt(1));
                System.out.printf(" menu_id %d\n", results.getInt(2));
                System.out.printf("name %s\n", results.getString(3));
                System.out.printf("Price %s\n", results.getDouble(4));
            }
        } catch (SQLException errr) {
        }
    }

    // Create a new table if doesnt exist
    public static void init() {
        Statement createTable;
        try {
            createTable = DB.conn.createStatement();
            createTable.execute(
                    "CREATE TABLE IF NOT EXISTS items (id INTEGER PRIMARY KEY, menu_id INTEGER, name TEXT, price FLOAT);");
            Statement getitem = DB.conn.createStatement();
            ResultSet item = getitem.executeQuery("Select * From items;");
            while (item.next()) {
                int id = item.getInt(1);
                int menu_id = item.getInt(2);
                String name = item.getString(3);
                Double price = item.getDouble(4);
                new Item(id, menu_id, name, price);
            }

        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
    }

    // Constructor for Item
    public Item(int menu_id, String name, Double price) {
        this.price = price;
        this.menu_id = menu_id;
        this.name = name;
        try {
            PreparedStatement insertitem = DB.conn
                    .prepareStatement("INSERT INTO items (menu_id,name,price) VALUES (?,?,?);");
            insertitem.setInt(1, this.menu_id);
            insertitem.setString(2, this.name);
            insertitem.setDouble(3, this.price);
            insertitem.executeUpdate();
            this.id = insertitem.getGeneratedKeys().getInt(1);
        } catch (SQLException err) {

        }
        Item.all.add(this);
    }

    public Item(int id, int menu_id, String name, Double price) {
        this.id = id;
        this.menu_id = menu_id;
        this.name = name;
        this.price = price;
        Item.all.add(this);
    }

    public int getItemID() {
        return this.id;
    }

    public String getItemName() {
        return this.name;
    }

    public Double getPrice() {
        return this.price;
    }

}
