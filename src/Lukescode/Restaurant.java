package Lukescode;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Restaurant {
    private int id;
    private String name;
    private String imageURL;
    public static ArrayList<Restaurant> all = new ArrayList<>();

    public static void getOneRestaurant() {
        try {
            PreparedStatement getOneRestaurant = DB.conn.prepareStatement("SELECT * FROM restaurants WHERE id = ?;");
            getOneRestaurant.setInt(1, 4);
            ResultSet chosenRestaurant = getOneRestaurant.executeQuery();
            System.out.printf("winnner is %s\n", chosenRestaurant.getString(2));
        } catch (SQLException err) {
        }
    }

    // DELETE
    public static void deleteOneRestaurant() {
        try {
            PreparedStatement deleteOneRestaurant = DB.conn.prepareStatement("DELETE FROM restaurants WHERE id = ?;");
            deleteOneRestaurant.setInt(1, 11);
            deleteOneRestaurant.executeUpdate();

        } catch (SQLException err) {

        }
    }

    // Select all from restaurant table
    public static void getAllRestaurants() {
        try {
            Statement getAllRestaurants = DB.conn.createStatement();
            ResultSet results = getAllRestaurants.executeQuery("Select * FROM restaurants;");
            while (results.next()) {
                System.out.printf(" id: %d\n", results.getInt(1));
                System.out.printf(" restaurant name %S\n", results.getString(2));
                System.out.printf("image url %s\n", results.getString(1));
            }
        } catch (SQLException errr) {
        }
    }

    public static void combineTables() {
        try {
            Statement getAllRestaurantDetails = DB.conn.createStatement();
            ResultSet results = getAllRestaurantDetails.executeQuery(
                    "SELECT restaurants.name, menus.title, items.name, items.price FROM restaurants JOIN menus ON menus.restaurant_id=restaurants.id JOIN items on items.menu_id=menus.id;");
            while (results.next()) {
                System.out.printf("restaurants name: %s\n", results.getString(1));
                System.out.printf("menu title: %s\n", results.getString(2));
                System.out.printf("items name: %s\n", results.getString(3));
                System.out.printf("items price: %s\n", results.getDouble(4));
            }
        } catch (SQLException err) {
        }
    }

    // Create a new table if doesnt exist
    public static void init() {
        Statement createTable;
        try {
            createTable = DB.conn.createStatement();
            createTable.execute(
                    "CREATE TABLE IF NOT EXISTS restaurants (id INTEGER PRIMARY KEY, name TEXT, imageURL TEXT);");
            Statement getRestaurants = DB.conn.createStatement();
            ResultSet restaurants = getRestaurants.executeQuery("Select * From restaurants;");
            while (restaurants.next()) {
                int id = restaurants.getInt(1);
                String name = restaurants.getString(2);
                String imageURL = restaurants.getString(3);
                new Restaurant(id, name, imageURL);
            }

        } catch (SQLException err) {
            System.out.println(err.getMessage());
        }
    }

    // Constructor for restaurant
    public Restaurant(String name, String imageURL) {
        this.name = name;
        this.imageURL = imageURL;
        try {
            PreparedStatement insertrestaurants = DB.conn
                    .prepareStatement("INSERT INTO restaurants (name,imageURL) VALUES (?,?);");
            insertrestaurants.setString(1, this.name);
            insertrestaurants.setString(2, this.imageURL);
            insertrestaurants.executeUpdate();
            this.id = insertrestaurants.getGeneratedKeys().getInt(1);
        } catch (Exception err) {

        }
        Restaurant.all.add(this);
    }

    public Restaurant(int id, String name, String imageURL) {
        this.id = id;
        this.name = name;
        this.imageURL = imageURL;
        Restaurant.all.add(this);
    }

    public String getRestaurantName() {
        return this.name;
    }

    public int getID() {
        return this.id;
    }

    public String getImage() {
        return this.imageURL;
    }

}
